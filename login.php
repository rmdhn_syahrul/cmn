<?php
	error_reporting(0);	
	session_start();
	if($_SESSION['username']){
	header("location:index.php");
}else{
?>

<html>
<head>
	<title>PT.CIPTAKOM MEDIA NUSA</title>
	<link href="css/bootstrap.css" rel="stylesheet">
<style type="text/css">
body{
	 .the-icons {
	      list-style: none outside none;
	      margin-left: 0;
	  }
	  .the-icons li {
	      float: left;
	      line-height: 25px;
	      width: 25%;
	  }
	  .the-icons i:hover {
	      background-color: rgba(255, 0, 0, 0.25);
	  }
/*
.btn{
	background-color: #339966 ;
	border-radius: 10px;
	color: #fff;
}
.btn:hover{
	background-color: #336666 ;
	cursor: pointer;
}*/
</style>
</head>
<body>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <span class="navbar-brand"><strong style="font-family: verdana; margin-left: 30px">PT.CMN (PT.Ciptakom Media Nusa)</strong></span>
          
        </div>
        
      </div>
    </div>
    <div class="container">
	
	<br><br>

	<div class="container-fluid" style="margin-top: 30px">
			
		<div class="panel panel-info" style="width: 400px; margin: 20px auto; border: solid 1px #d9d9d9; padding: 30px 20px; border-radius: 8px">
		  <div class="panel-heading">
		    <strong><h3 class="panel-title">Login!</h3></strong>
		  </div>
		  <div class="panel-body">
	<div id="isi">
		<form action="login-submit.php" method="post">
			<div>
				<input class="form-control" type="text" name="username" placeholder="Username" autofocus>
			</div>
			<div style="margin-top: 10px;">
				<input class="form-control" type="password" name="password" placeholder="Password">
			</div>
			<div style="margin-top: 10px;">
				<button type="submit" name="btnSubmit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</button>
		        <br/>
		        <center> <!-- <input class="btn" type="submit" name="btnSubmit" value="Login"> -->
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" title="Kontak Kami"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Kontak Kami</button>
			    </center>

			</div>
		</form>

		
	</div>
</div>
</div><!--/row-->
      

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Kontak Kami</h4>
        </div>
        <div class="modal-body">
          <p><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Cengkareng - Pedongkelan Depan </p>
          <p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Tlp: 021-2233-3342 </p>
          <p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> masdedy@gmail.com </p>
          <!--<p><i class="icon-blackberry"></i> 77xxxx </p>-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

    </div><!--/.fluid-container-->
	<center style="margin-top: -15px;">&copy; 2020&nbsp;by <a> PT.CMN </a> <br>
	</center>

</body>
</html>
<?php 
}
?>