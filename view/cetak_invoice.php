<?php
include "./inc/function.php";
include "./inc/config.php";
$perusahaan = mysqli_query($connect, "SELECT * FROM t_setting limit 1") or die (mysqli_error($connect));
$seting = mysqli_fetch_array($perusahaan);
$id_transaksi=$_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Cetak Invoice | PT.Ciptakom Media Nusa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="../css/default/bootstrap.css" />
    <link href="../css/custom-style.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <style type="text/css">
        .ttl-amts { text-align: right}
    </style>
</head>
<body onLoad="window.print()">
    <?php
        include "../inc/config.php";
        include "../inc/function.php";
        $query = "SELECT t_tagihan.*, t_pelanggan.*, t_paket.* from t_tagihan left join t_pelanggan on t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan left join t_paket on t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.id_tagihan='$id_transaksi'";
        $result = mysqli_query($connect, $query) or die(mysqli_error($connect));
        while ($data = mysqli_fetch_array($result)){
    ?>
    <hr />
    <div class="row contact-info">
        <div class="col-lg-6 col-md-6 col-sm-6">
                <h4>
                    <strong>#<?php echo $data['id_tagihan'] ?> </strong>
                </h4>
                <hr />
                Tanggal Tagihan :  <?php echo TanggalIndo($data['tanggal']); ?>
                <br />         
                <b>Status :  <?php echo ucfirst($data['status_tagihan']) ?> </b>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
                <h4>  <strong>Data Pelanggan </strong></h4>
                <hr/>
                <b>Nama :</b> <?php echo $data['nama'] ?>.
                <br />
                <b>Alamat :</b> <?php echo $data['alamat'] ?>.
                <br />
                <b>No HP :</b> <?php echo $data['no_hp'] ?>
                <br />
                <b>E-mail :</b> <?php echo $data['email'] ?>
        </div>
    </div>
            <hr />

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Description</th>
                        <!-- <th>Speed.</th> -->
                        <th>Harga Paket</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td><?php echo $data['nama_paket'] ?></td>
                        <!-- <td><?php echo $data['nama_paket'] ?></td> -->
                        <td><?php echo number_format( $data['harga_paket'] , 0 , ',' , '.' ); ?></td>
                        <td><?php echo number_format( $data['harga_paket'] , 0 , ',' , '.' ); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="ttl-amts">
                <h5>  Total : <?php echo number_format( $data['harga_paket'] , 0 , ',' , '.' ); ?> </h5>
            </div>
            <hr />
            <div class="ttl-amts">
                <h5>  Sudah termasuk PPN 10% </h5>
            </div>
            <hr />
            <div class="ttl-amts">
                <h4> <strong>Total : <?php echo number_format( $data['harga_paket'] , 0 , ',' , '.' ); ?></strong> </h4>
            </div>
        </div>
    </div>
<?php 
};
?>
</body>
</html>