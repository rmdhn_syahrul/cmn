<?php
include "./inc/function.php";
$id = $_SESSION['id'];
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active"><?php echo ucfirst($page); ?></li>
</ul>
<?php if ($_SESSION['level'] == 'admin') {; ?>
  <div class="btn-group">
    <a href="?page=transaksi&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
  </div>
<?php } ?>
<?php if ($_SESSION['level'] == 'pelanggan') {; ?>
  <div class="btn-group">
    <a href="?page=transaksi&aksi=tambah&id=<?php echo $id; ?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
  </div>
<?php } ?>

<br /><br />
<?php
if ($action == "") {
?>

  <div class="table-responsive">
    <table class="table table-bordered" id="tabel-transaksi">
      <thead>
        <tr class="info">
          <!-- <th>#</th> -->
          <th>ID Transaksi</th>
          <th>ID Tagihan</th>
          <!-- <th>Tgl Tagihan</th> -->
          <!-- <th>ID Pelanggan</th> -->
          <th>Nama Pelanggan</th>
          <!-- <th>Nama Paket</th> -->
          <!-- <th>Harga Paket</th> -->
          <th>Tgl Bayar</th>
          <!-- <th>Total Tagihan</th> -->
          <th>Total Bayar</th>
          <!-- <th>Tgl Validasi</th> -->
          <th>Status</th>
          <th>File</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody align="center"></tbody>
    </table>
  </div>

<?php
} else if ($action == "delete") {
  $hapus = mysqli_query($connect, "DELETE from t_transaksi WHERE id_transaksi='$_GET[id]'") or die(mysqli_error($connect));
  echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=transaksi">';
  break;
} else {
  echo "maaf aksi tidak ditemukan";
}
?>

<!-- datatables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    fetch_data();
    function fetch_data() {
      var tabel = $('#tabel-transaksi').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "order": [
          [0, 'asc']
        ],
        "ajax": {
          "url": "./view/datatables_transaksi.php",
          "type": "POST"
        },
        "deferRender": true,
        "aLengthMenu": [
          [5, 10, 50],
          [5, 10, 50]
        ],
        "columns": [{
            "data": "id_transaksi"
          }, {
            "data": "id_tagihan"
          },
          {
            "data": "nama"
          },
          {
            "data": "tgl_bayar"
          },
          {
            "data": "nominal_bayar"
          },
          {
            "data": "status_transaksi"
          },
          {
            "data": "bukti"
          },
          {
            "render": function(data, type, row) {
              var html = "<a href=\"?page=transaksi&aksi=detail&id=" + row.id_transaksi + " \"class=\"btn btn-success btn-sm\" title=\"Lihat Data\"><span class=\"glyphicon glyphicon-new-window\" aria-hidden=\"true\"></span></a>";
              html += "<a href=\"?page=transaksi&aksi=edit&id=" + row.id_transaksi + " \"class=\"btn btn-info btn-sm\" title=\"Edit Data\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a>";
              html += "<a href=\"?page=transaksi&aksi=delete&id="+row.id_transaksi+" \"class=\"btn btn-danger btn-sm\" title=\"Delete Data\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a>";  
              html += "<a href=\"?page=cetak_bukti_bayar&id=" + row.id_transaksi + " \"class=\"btn btn-info btn-sm\" title=\"Print Data\"><span class=\"glyphicon glyphicon-print\" aria-hidden=\"true\"></span></a>";
              return html
            }
          },
        ],
        columnDefs: [{
          className: 'text-center',
          targets: [1, 5, 6]
        }, {
          className: 'text-right',
          targets: [4]
        }, {
          "targets": [5, 6],
          "orderable": false,
        }]
      });
    }
  });
</script>