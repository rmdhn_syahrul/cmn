<?php
include "./inc/function.php";
$id = $_SESSION['id'];
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active"><?php echo ucfirst($page) ; ?></li>
</ul>
  <?php if($_SESSION['level'] == 'admin'){ ;?>
<div class="btn-group" >
  <a href="?page=tagihan&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
</div>
<?php }?>
<?php if ($_SESSION['level']== 'pelanggan') { ;?>
  <div class="btn-group" >
  <a href="?page=tagihan&aksi=tambah&id=<?php echo $id ;?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
</div>
<?php }?>
<br/><br/>
<?php 
  if ($action == ""){
?>
<table class="table table-hover table-bordered table-striped">
  <thead>
      <tr class="info">
        <th>#</th>
        <th>ID Tagihan</th>
        <th>Tanggal Tagihan</th>
        <!-- <th>ID Pelanggan</th> -->
        <th>Nama Pelanggan</th>
        <th>Nama Paket</th>
        <th>Harga Paket</th>   
        <th>Status</th>                       
        <th>Aksi</th>       
      </tr>
    </thead>
    <tbody>
      <?php
        include "./inc/config.php";
        if($_SESSION['level'] == 'admin'){
        $query=mysqli_query($connect, "SELECT t_tagihan.id_tagihan, t_tagihan.tanggal, t_tagihan.status_tagihan, t_pelanggan.id_pelanggan, t_pelanggan.nama, t_paket.nama_paket, t_paket.harga_paket FROM t_tagihan JOIN t_pelanggan ON t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan JOIN t_paket ON t_tagihan.id_paket=t_paket.id_paket order by t_tagihan.id_tagihan ASC") or die (mysqli_error($connect));  //mengambil data tabel mahasiswa dan memasukkan nya ke variabel query        
        }else{
        $query=mysqli_query($connect, "SELECT t_tagihan.id_tagihan, t_tagihan.tanggal, t_tagihan.status_tagihan, t_pelanggan.id_pelanggan, t_pelanggan.nama, t_paket.nama_paket, t_paket.harga_paket FROM t_tagihan JOIN t_pelanggan ON t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan JOIN t_paket ON t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.id_pelanggan='$_SESSION[id]' order by t_tagihan.id_tagihan ASC ") or die (mysqli_error($connect));  //mengambil data tabel mahasiswa dan memasukkan nya ke variabel query
        }
        $no=1;
        while($lihat=mysqli_fetch_array($query, MYSQLI_ASSOC)){
        ?>    
      <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $lihat['id_tagihan'] ?></td>
        <td><?php echo TanggalIndo ($lihat['tanggal']); ?></td>
        <!-- <td><?php echo $lihat['id_pelanggan'] ?></td> -->
        <td><?php echo $lihat['nama'] ?></td>
        <td><?php echo $lihat['nama_paket'] ?></td>
        <td><?php echo "Rp." . number_format ($lihat['harga_paket'], 0, ',' , '.'); ?></td>
          <td><?php if ($lihat['status_tagihan']=='LUNAS'){ ?>
          <span class="label label-success"><?php echo ucfirst($lihat['status_tagihan']) ?></span>
          <?php }else{ ?>
          <span class="label label-danger"><?php echo ucfirst($lihat['status_tagihan']) ?></span>
          <?php }?>
        </td> 
       <td align="center">
       <a href="?page=tagihan&aksi=detail&id=<?php echo $lihat['id_tagihan'] ;?>" class="btn btn-success btn-sm" title="Lihat Data"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
        <?php if($_SESSION['level'] == 'admin'){ ;?>
          <a href="?page=tagihan&aksi=edit&id=<?php echo $lihat['id_tagihan'] ;?>" class="btn btn-info btn-sm" title="Edit Data"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> 
          <a href="?page=tagihan&aksi=delete&id=<?php echo $lihat['id_tagihan'] ;?>" onclick="javascript: return confirm('Anda yakin akan menghapus data ini ?')" class="btn btn-danger btn-sm" title="Hapus Data"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        <?php }?>
        <a href="?page=cetak_invoice&id=<?php echo $lihat['id_tagihan'] ;?>" target="_blank" class="btn btn-info btn-sm" title="Cetak"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a> 
      </td>
                
      </tr>
      <?php
        }
      ?>
    </tbody>

</table>
  <ul class="pagination">
    <li class="disabled"><a href="#">«</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">»</a></li>
  </ul>
<?php
}else if($action == "delete"){
$hapus=mysqli_query($connect, "DELETE from t_tagihan WHERE id_tagihan='$_GET[id]'") or die(mysqli_error($connect));
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=tagihan">';
break;
}else{
  echo "maaf aksi tidak ditemukan";
}
?>
