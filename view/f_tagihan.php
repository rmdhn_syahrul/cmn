<?php
include "./inc/config.php";
include "./inc/function.php";

?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>

<form class="form-horizontal" method="POST">
  <fieldset>
    <legend>Tambah Data Tagihan</legend>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Tagihan</label>
      <div class="col-sm-3">
        <?php
          $query =mysqli_query($connect, "SELECT max(id_tagihan) as maxCode from t_tagihan");
          $data = mysqli_fetch_array($query);
          $maxData = $data['maxCode'];

          $urutan = (int) substr($maxData, 4, 3);
          $urutan++;

          $huruf = "INV-";
          $kodePelanggan = $huruf . sprintf("%03s", $urutan);
				?>
	    	<input type="text" name="id" required="required" class="form-control" value="<?php echo $kodePelanggan;?>" readonly >
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal</label>
      <div class="col-sm-3">
         <input type="text" id="datepicker" class="form-control" name="tanggal" value="<?php echo $lihat['tanggal'] ;?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Kode Pelanggan</label>
      <div class="col-sm-3">
        <!-- <input type="text" class="form-control" name="id" required placeholder="Kode Pelanggan"> -->
        <select name="kodePelanggan" onchange="showUser(this.value)" class="form-control" id="searchKodePelanggan">
          <option value="">--Pilih Pelanggan--</option>
          <?php
            if($_SESSION['level'] == 'admin'){
              $pos=mysqli_query($connect, "select id_pelanggan, nama from t_pelanggan WHERE status = 'aktif' order by id_pelanggan");
            } else {
              $pos=mysqli_query($connect, "select id_pelanggan, nama from t_pelanggan WHERE status = 'aktif' and id_pelanggan='$_GET[id]'");
            }
            while($r_pos=mysqli_fetch_array($pos)){
              echo "<option value=\"$r_pos[id_pelanggan]\" data-value='{\"id\": \"$r_pos[id_pelanggan]\", \"name\": \"$r_pos[nama]\"}'>$r_pos[id_pelanggan]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Pelanggan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_pelanggan" placeholder="Nama Pelanggan" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Paket</label>
      <div class="col-sm-3">
        <select name="kodePaket" onchange="showUser(this.value)" class="form-control" id="searchNamaPaket">
          <option value="" data-value="">--Pilih Paket--</option>
          <?php
            $pos=mysqli_query($connect, "select id_paket, nama_paket, harga_paket from t_paket order by nama_paket");
            while($r_pos=mysqli_fetch_array($pos)){
              echo "<option value=\"$r_pos[id_paket]\" data-value='{\"id\": \"$r_pos[id_paket]\", \"name\": \"$r_pos[nama_paket]\", \"harga_paket\": \"$r_pos[harga_paket]\"}'>$r_pos[nama_paket]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tagihan</label>
      <div class="col-sm-3">
          <input type="text" class="form-control" name="total_tagihan" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Keterangan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="keterangan" placeholder="Keterangan">
      </div>
    </div>  
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
      <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tambah</button>
        <a href="?page=tagihan" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
      </div>
    </div>
  </fieldset>
</form>

  <?php 
  if(isset($_POST['simpan'])){
    $cekdata="SELECT id_tagihan from t_tagihan where id_tagihan='".$_POST['id']."'"; 
    $ada=mysqli_query($connect, $cekdata) or die(mysqli_error($connect)); 
    if(mysqli_num_rows($ada)>0) { 
      writeMsg('tagihan.sama');
    } else { 
      $query="INSERT INTO t_tagihan VALUES ('".$_POST['id']."','".$_POST['kodePelanggan']."','".$_POST['kodePaket']."','".$_POST['tanggal']."','PENDING','".$_POST['total_tagihan']."','".$_POST['keterangan']."')"; 
      mysqli_query($connect, $query) or die("Gagal menyimpan data karena :").mysqli_error($connect); 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=tagihan">';
    } 
  } 

  ?>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
      });

      $("#searchKodePelanggan").chosen().change(function() {
        $("input[name*='nama_pelanggan']").val($('#searchKodePelanggan option:selected').data("value").name);
      });

      $("#searchNamaPaket").chosen().change(function() {
        $("input[name*='total_tagihan']").val($('#searchNamaPaket option:selected').data("value").harga_paket);
      });
    })
  </script>