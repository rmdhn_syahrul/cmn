<?php
include "./inc/function.php";
$id = $_SESSION['id'];
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active"><?php echo ucfirst($page) ; ?></li>
</ul>
  <?php if($_SESSION['level'] == 'admin'){ ;?>
<div class="btn-group" >
  <a href="?page=tagihan&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
</div>
<?php }?>
<?php if ($_SESSION['level']== 'pelanggan') { ;?>
<label class="label-danger"><strong>* Abaikan Pesan Ini Jika Sudah Melakukan Pembayaran!! </strong></label>
<?php }?>
<br/><br/>
<?php 
  if ($action == ""){
?>
<table class="table table-hover table-bordered table-striped">
  <thead>
      <tr class="info">
        <th>#</th>
        <th>ID Tagihan</th>
        <th>Nama Pelanggan</th>
        <th>Tanggal</th>
        <th>Keterangan</th>
        <th>Harga Paket</th>                          
        <th>Aksi</th>       
      </tr>
    </thead>
    <tbody align="center">
      <?php
        include "./inc/config.php";
        if($_SESSION['level'] == 'admin'){
        $query=mysqli_query($connect, "SELECT * from t_transaksi order by id_transaksi ASC") or die (mysqli_error($connect));  //mengambil data tabel mahasiswa dan memasukkan nya ke variabel query        
        }else{
        $query=mysqli_query($connect, "SELECT * from t_tagihan WHERE id_tagihan='$_SESSION[id]' order by id_tagihan ASC ") or die (mysqli_error($connect));  //mengambil data tabel mahasiswa dan memasukkan nya ke variabel query
        }
        $no=1;                    //membuat nomor pada tabel
        while($lihat=mysqli_fetch_array($query)){    //mengeluarkan isi data dengan mysql_fetch_array dengan perulangan
        ?>    
      <tr>
        <td><?php echo $lihat['id_transaksi'] ?></td>
        <td><?php echo $lihat['id_tagihan'] ?></td>
        <td><?php echo $lihat['nama'] ?></td>
        <td><?php echo TanggalIndo ($lihat['tgl_bayar']); ?></td>
        <td><?php echo "Rp." . number_format ($lihat['nominal_bayar'], 0, ',' , '.'); ?></td>
        <td>
          <?php if ($lihat['status_transaksi']=='LUNAS'){ ?>
            <span class="label label-success"><?php echo ucfirst($lihat['status_transaksi']) ?></span>
            <?php }else{ ?>
            <span class="label label-danger"><?php echo ucfirst($lihat['status_transaksi']) ?></span>
          <?php }?>
        </td> 
       <td align="center">
        <?php if($_SESSION['level'] == 'pelanggan'){ ;?>
         <a href="?page=tagihan&aksi=detail&id=<?php echo $lihat['id_tagihan'] ;?>" class="btn btn-success btn-sm" title="Lihat Data"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
         <?php }?>
        <?php if($_SESSION['level'] == 'admin'){ ;?>
          <a href="?page=tagihan&aksi=edit&id=<?php echo $lihat['id_tagihan'] ;?>" class="btn btn-info btn-sm" title="Edit Data"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> 
          <a href="?page=tagihan&aksi=delete&id=<?php echo $lihat['id_tagihan'] ;?>" onclick="javascript: return confirm('Anda yakin akan menghapus data ini ?')" class="btn btn-danger btn-sm" title="Hapus Data"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        <?php }?>
      </td>
                
      </tr>
      <?php
        }
      ?>
    </tbody>

</table>
  <ul class="pagination">
    <li class="disabled"><a href="#">«</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">»</a></li>
  </ul>
<?php
}else if($action == "delete"){
$hapus=mysqli_query($connect, "DELETE from t_tagihan WHERE id_tagihan='$_GET[id]'") or die(mysqli_error($connect));
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=tagihan">';
break;
}else{
  echo "maaf aksi tidak ditemukan";
}
?>
