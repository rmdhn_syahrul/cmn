<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Transaksi</li>
</ul>
 <?php
 include "./inc/config.php";
 $query=mysqli_query($connect, "SELECT t_transaksi.*, t_tagihan.*, t_pelanggan.*, t_paket.* from t_transaksi left join t_tagihan on t_transaksi.id_tagihan=t_tagihan.id_tagihan left join t_pelanggan on t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan left join t_paket on t_tagihan.id_paket=t_paket.id_paket WHERE t_transaksi.id_transaksi='$_GET[id]'");
 $result=mysqli_fetch_array($query);
 ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Detail Transaksi</h3>
  </div>
  <div class="panel-body">
	<div class="form-horizontal" role="form">
		<div class="form-group">
			<label class="col-sm-2 control-label">ID Bayar</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['id_transaksi']; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">ID Tagihan</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['id_tagihan']; ?></label>
			</div>
		</div>			  					  
		<div class="form-group">
		<label class="col-sm-2 control-label">Nama</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo $result['nama']; ?></label>
		</div>
		</div>					  
		<div class="form-group">
		<label class="col-sm-2 control-label">Alamat</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo $result['alamat']; ?></label>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">No. Telp</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo $result['no_hp']; ?></label>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo $result['email']; ?></label>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Jenis Paket</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo $result['nama_paket']; ?></label>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Total Tagihan</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo "Rp.".number_format($result['harga_paket'], 0, ',', '.'); ?></label>
		</div>
	</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Total Bayar</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<label class="col-sm-0 control-label"><?php echo "Rp.".number_format($result['nominal_bayar'], 0, ',', '.'); ?></label>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Status</label>
		<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<?php if ($result['status_transaksi']=='LUNAS'){ ?>
				<span class="col-sm-0 control-label label label-success"><?php echo ucfirst($result['status_transaksi']) ?></span>
				<?php }else{ ?>
				<span class="col-sm-0 control-label label label-danger"><?php echo ucfirst($result['status_transaksi']) ?></span>
			<?php }?>
		</div>
		</div>
		<div class="btn-group pull-right">
		<?php 
			echo "<a href=\"?page=transaksi\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-arrow-left\"></span> Kembali</a>"; 
		?> 
		</div>
	</div>
  </div>
</div>


					