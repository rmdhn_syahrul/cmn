<?php
session_start();
include "../inc/function.php";
include "../inc/config.php"; // Load file koneksi.php

$search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
$limit = $_POST['length']; // Ambil data limit per page
$start = $_POST['start']; // Ambil data start

$sql = mysqli_query($connect, "SELECT id_transaksi FROM t_transaksi"); // Query untuk menghitung seluruh data tagihan
$sql_count = mysqli_num_rows($sql); // Hitung data yg ada pada query $sql

$query = "SELECT t_transaksi.*, t_tagihan.*, t_pelanggan.*, t_paket.* FROM t_transaksi
    LEFT JOIN t_tagihan ON t_tagihan.id_tagihan = t_transaksi.id_tagihan
    LEFT JOIN t_pelanggan ON t_pelanggan.id_pelanggan = t_tagihan.id_pelanggan
    LEFT JOIN t_paket ON t_paket.id_paket = t_tagihan.id_paket ";
if($_SESSION['level'] == 'admin'){
    $query .= "WHERE (t_transaksi.id_transaksi LIKE '%".$search."%' OR t_transaksi.id_tagihan LIKE '%".$search."%' OR t_pelanggan.nama LIKE '%".$search."%')";
} else if($_SESSION['level'] == 'pelanggan') {
    $query .= "WHERE t_tagihan.id_pelanggan= '".$_SESSION['id']."' AND (t_transaksi.id_transaksi LIKE '%".$search."%' OR t_transaksi.id_tagihan LIKE '%".$search."%' OR t_pelanggan.nama LIKE '%".$search."%')";
}
$order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
$order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
$order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
$order = " ORDER BY ".$order_field." ".$order_ascdesc;

$sql_data = mysqli_query($connect, $query.$order." LIMIT ".$limit." OFFSET ".$start); // Query untuk data yang akan di tampilkan
$sql_filter = mysqli_query($connect, $query); // Query untuk count jumlah data sesuai dengan filter pada textbox pencarian
$sql_filter_count = mysqli_num_rows($sql_filter); // Hitung data yg ada pada query $sql_filter

$data = mysqli_fetch_all($sql_data, MYSQLI_ASSOC); // Untuk mengambil data hasil query menjadi array
$callback = array(
    'draw'=>$_POST['draw'], // Ini dari datatablenya
    'recordsTotal'=>$sql_count,
    'recordsFiltered'=>$sql_filter_count,
    'data'=>$data
);

header('Content-Type: application/json');
echo json_encode($callback); // Convert array $callback ke json
?>