<?php
include "./inc/config.php";
include "./inc/function.php";

?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>

<form class="form-horizontal" method="POST">
  <fieldset>
    <legend>Tambah Data Area</legend>
    <div class="form-group">
      <label class="col-sm-2 control-label">Kode Area</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_area" required placeholder="Kode Area">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Area</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_area" placeholder="Nama Area">
      </div>
    </div>
    
    
   <input type="hidden" name="info" value="1">
    <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
        <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tambah</button>
        <a href="?page=area" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
      </div>
    </div>
  </fieldset>
</form>

  <?php 
  if(isset($_POST['simpan'])){
    $cekdata="SELECT id_area from area where id_area='".$_POST['id_area']."'";
    $ada=mysqli_query($connect, $cekdata) or die(mysqli_error($connect)); 
    $data="SELECT * from area";
    $aya=mysqli_query($connect, $data) or die(mysqli_error($connect));
    if(mysqli_num_rows($ada)>0) { 
      writeMsg('area.sama');
    } else { 
      $query="INSERT INTO area (id_area, nama_area) VALUES ('".$_POST['id_area']."','".$_POST['nama_area']."')";
      mysqli_query($connect, $query) or die("Gagal menyimpan data karena :") or die(mysqli_error($connect)); 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=area">';
    } 
  } 

  ?>