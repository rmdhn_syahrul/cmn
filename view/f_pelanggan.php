<?php
session_start();
include "./inc/function.php";
include "./inc/config.php";
if ($_SESSION['level'] == "operator") {
	header("location:index.php");
} else {
?>
	<ul class="breadcrumb">
		<li><a href="./">Home</a></li>
		<li><a href="?page=<?php echo $page; ?>"><?php echo ucfirst($page); ?></a></li>
		<li class="active"><?php echo ucfirst($action); ?> Data</li>
	</ul>
	<fieldset>
		<legend>Tambah Data Pelanggan</legend>
		<form class="form-horizontal" method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">ID Pelanggan</label>
				<div class="col-sm-3">
					<?php
					$result = mysqli_query($connect, "SELECT * from t_pelanggan");
					$num_rows = mysqli_num_rows($result);
					$number = str_pad($num_rows, 3, '0', STR_PAD_LEFT);
					$num_rows++;

					$huruf = "PEL";
					$kodePelanggan = $huruf . sprintf("%03s", $number);
					?>
					<input type="text" name="id" required="required" class="form-control" value="<?php echo $kodePelanggan; ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama</label>
				<div class="col-sm-4">
					<input type="text" name="nama" class="form-control" placeholder="Nama">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Alamat</label>
				<div class="col-sm-5">
					<textarea class="form-control" name="alamat" rows="3"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">No. Telp</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="telpon" placeholder="No. Telp">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="email" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-2">
					<select name="status" onchange="showUser(this.value)" class="form-control">
						<option value="">--Pilih Status--</option>
						<option value="aktif">Aktif</option>
						<option value="non aktif">Non Aktif</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
					<button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tambah</button>
					<a href="?page=pelanggan" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
				</div>
			</div>
		</form>
	</fieldset>

	<?php
	if (isset($_POST['simpan'])) {
		$cekdata = "SELECT id_pelanggan from t_pelanggan where id_pelanggan='" . $_POST['id'] . "'";
		$ada = mysqli_query($connect, $cekdata) or die(mysqli_error($connect));
		if (mysqli_num_rows($ada) > 0) {
			writeMsg('pelanggan.sama');
		} else {
			$query = "INSERT INTO t_pelanggan VALUES ('" . $_POST['id'] . "','" . $_POST['nama'] . "','" . $_POST['alamat'] . "','" . $_POST['telpon'] . "','" . $_POST['email'] . "','" . $_POST['status'] . "')";
			mysqli_query($connect, $query) or die("Gagal menyimpan data karena :") . mysqli_error($connect);
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=pelanggan">';
		}
	}

	?>

<?php
}
?>