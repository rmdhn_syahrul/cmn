<?php
error_reporting(0);
session_start();
include "./inc/function.php";
$_SESSION['info'];
if ($_SESSION['level'] == "pelanggan") {
  header("location:index.php");
} else {
?>
  <ul class="breadcrumb">
    <li><a href="./">Home</a></li>
    <li class="active"><?php echo ucfirst($page); ?></li>
  </ul>
  <div class="btn-group">
    <a href="?page=paket&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
  </div>

  <br /><br />
  <?php
  if ($action == "") {
  ?>
  <div class="table-responsive">
  <table class="table table-bordered" id="tabel-paket">
      <thead>
        <tr class="info">
          <!-- <th>#</th> -->
          <th>Kode Paket</th>
          <th>Nama Paket</th>
          <th>Harga Paket</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
  <?php
  } else if ($action == "delete") {
    $hapus = mysqli_query($connect, "DELETE from t_paket WHERE id_paket='$_GET[id]'") or die(mysqli_error($connect));
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=paket">';
    break;
  } else {
    echo "maaf aksi tidak ditemukan";
  }
  ?>
<?php
}
?>

<!-- datatables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    fetch_data();
    function fetch_data() {
      var tabel = $('#tabel-paket').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "order": [
          [0, 'asc']
        ],
        "ajax": {
          "url": "./view/datatables_paket.php",
          "type": "POST"
        },
        "deferRender": true,
        "aLengthMenu": [
          [5, 10, 50],
          [5, 10, 50]
        ],
        "columns": [
          {
            "data": "id_paket"
          }, {
            "data": "nama_paket"
          },
          {
            "data": "harga_paket"
          },
          {
            "render": function(data, type, row) {
              var html = "<a href=\"?page=paket&aksi=edit&id=" + row.id_paket + " \"class=\"btn btn-info btn-sm\" title=\"Edit Data\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a>";
              html += "<a href=\"?page=paket&aksi=delete&id="+row.id_paket+" \"class=\"btn btn-danger btn-sm\" title=\"Delete Data\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a>";
              return html
            }
          },
        ],
        columnDefs: [{
          className: 'text-center',
          targets: [3]
        }, {
          className: 'text-right',
          targets: [2]
        }, {
          "targets": [3],
          "orderable": false,
        }]
      });
    }
  });
</script>