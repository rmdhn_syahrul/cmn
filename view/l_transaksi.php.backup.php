<?php
include "./inc/function.php";
$id = $_SESSION['id'];
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active"><?php echo ucfirst($page) ; ?></li>
</ul>
<?php if($_SESSION['level'] == 'admin'){ ;?>
<div class="btn-group" >
  <a href="?page=transaksi&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
</div>
<?php }?>
<?php if ($_SESSION['level']== 'pelanggan') { ;?>
  <div class="btn-group" >
  <a href="?page=transaksi&aksi=tambah&id=<?php echo $id ;?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
</div>
<?php }?>

<br/><br/>
<?php 
  if ($action == ""){
?>

<div class="table-responsive">
    <table class="table table-bordered" id="tabel-tagihan">
	<thead>
	    <tr class="info">
	      <th>#</th>
	      <th>ID Transaksi</th>
	      <th>ID Tagihan</th>
	      <th>Total Tagihan</th>
	      <!-- <th>Tgl Tagihan</th> -->
        <!-- <th>ID Pelanggan</th> -->
        <th>Nama Pelanggan</th>
        <!-- <th>Nama Paket</th> -->
        <!-- <th>Harga Paket</th> -->
	      <th>Tgl Bayar</th>
	      <th>Total Bayar</th>
	      <!-- <th>Tgl Validasi</th> -->
	      <th>Status</th>
        <th>File</th>                          
	      <th>Aksi</th>       
	    </tr>
  	</thead>
  	<tbody align="center">
  		<?php
        include "./inc/config.php";
        if($_SESSION['level'] == 'admin'){
          $query=mysqli_query($connect, "SELECT
              t_transaksi.*,
              t_tagihan.*,
              t_pelanggan.*,
              t_paket.*
          FROM
              t_transaksi
          LEFT JOIN t_tagihan ON t_transaksi.id_tagihan = t_tagihan.id_tagihan
          LEFT JOIN t_pelanggan ON t_tagihan.id_pelanggan = t_pelanggan.id_pelanggan
          LEFT JOIN t_paket ON t_tagihan.id_paket = t_paket.id_paket
          order by id_transaksi ASC") or die (mysqli_error($connect));       
        }else{
          $query=mysqli_query($connect, "SELECT
              t_transaksi.*,
              t_tagihan.*,
              t_pelanggan.*,
              t_paket.*
          FROM
              t_transaksi
          LEFT JOIN t_tagihan ON t_transaksi.id_tagihan = t_tagihan.id_tagihan
          LEFT JOIN t_pelanggan ON t_tagihan.id_pelanggan = t_pelanggan.id_pelanggan
          LEFT JOIN t_paket ON t_tagihan.id_paket = t_paket.id_paket
          WHERE
              t_tagihan.id_pelanggan = '$_SESSION[id]'
          order by t_transaksi.id_transaksi ASC ") or die (mysqli_error($connect));
        }
        $no=1;
        while($lihat=mysqli_fetch_array($query)){
        ?>    
      <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $lihat['id_transaksi'] ?></td>
        <td><?php echo $lihat['id_tagihan'] ?></td>
        <td><?php echo "Rp." . number_format( $lihat['harga_paket'] , 0 , ',' , '.' ); ?></td>
        <!-- <td><?php echo $lihat['tanggal'] ?></td> -->
        <!-- <td><?php echo $lihat['id_pelanggan'] ?></td> -->
        <td><?php echo $lihat['nama'] ?></td>
        <!-- <td><?php echo $lihat['nama_paket'] ?></td> -->
        <!-- <td><?php echo "Rp." . number_format( $lihat['harga_paket'] , 0 , ',' , '.' ); ?></td> -->
        <td><?php echo TanggalIndo($lihat['tgl_bayar']); ?></td>
        <td><?php echo "Rp." . number_format( $lihat['nominal_bayar'] , 0 , ',' , '.' ); ?></td>
        <!-- <td><?php
        if (!$lihat['tgl_validasi']){
        echo "<span class='label label-warning'>".ucwords("belum validasi")."</span>";
        }else{
        echo TanggalIndo($lihat['tgl_validasi']); 
        }?></td> -->
        <td><?php if ($lihat['status_transaksi']=='LUNAS'){ ?>
          <span class="label label-success"><?php echo ucfirst($lihat['status_transaksi']) ?></span>
          <?php }else{ ?>
          <span class="label label-danger"><?php echo ucfirst($lihat['status_transaksi']) ?></span>
          <?php }?>
        </td>
        <td><?php echo $lihat['bukti'] ?></td>
        
        <td align="center">          
  				<a href="?page=transaksi&aksi=detail&id=<?php echo $lihat['id_transaksi'] ;?>" class="btn btn-success btn-sm" title="Lihat Data"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a> 
  				<?php if($_SESSION['level'] == 'admin'){ ;?>
            <a href="?page=transaksi&aksi=edit&id=<?php echo $lihat['id_transaksi'] ;?>" class="btn btn-info btn-sm" title="Edit Data"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> 
  				  <a href="?page=transaksi&aksi=delete&id=<?php echo $lihat['id_transaksi'] ;?>" onclick="javascript: return confirm('Anda yakin akan menghapus data ini ?')" class="btn btn-danger btn-sm" title="Hapus Data"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>  		    
          <?php }?>
          <?php if($lihat['status_transaksi']=='LUNAS'){
          ?>
          <a href="?page=cetak_bukti_bayar&id=<?php echo $lihat['id_transaksi'] ;?>" name="cetak" target="_blank" class="btn btn-info btn-sm" title="Cetak"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
          <?php
          }
          ?>
        </td>      
      </tr>
      <?php
        }
      ?>
  	</tbody>

</table>
</div>

<?php
}else if($action == "delete"){
$hapus=mysqli_query($connect, "DELETE from t_transaksi WHERE id_transaksi='$_GET[id]'") or die(mysqli_error($connect));
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=transaksi">';
break;
}else{
  echo "maaf aksi tidak ditemukan";
}
?>
