<?php
include "./inc/config.php";
include "./inc/function.php";
$id = $_SESSION['id'];

?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>
<?php
  $query=mysqli_query($connect, "SELECT t_tagihan.id_tagihan, t_tagihan.tanggal, t_tagihan.keterangan, t_pelanggan.id_pelanggan, t_pelanggan.nama, t_paket.id_paket, t_paket.nama_paket, t_paket.harga_paket FROM t_tagihan JOIN t_pelanggan ON t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan JOIN t_paket ON t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.id_tagihan='$_GET[id]' " ) or die (mysqli_error($connect));
  while($lihat=mysqli_fetch_array($query)){
?> 
<form class="form-horizontal" method="POST">
  <fieldset>
    <legend>Update Data Tagihan</legend>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Tagihan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_tagihan" required value="<?php echo $lihat['id_tagihan'] ;?>" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal</label>
      <div class="col-sm-3">
         <input type="text" id="datepicker" class="form-control" name="tanggal" value="<?php echo $lihat['tanggal'] ;?>" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Pelanggan</label>
      <div class="col-sm-3">
        <!-- <input type="text" class="form-control" name="id_pelanggan" value="<?php echo $lihat['id_pelanggan'] ;?>"> -->
        <select name="kodePelanggan" onchange="showUser(this.value)" class="form-control" id="searchKodePelanggan">
          <option value="" data-value="">--Pilih Pelanggan--</option>
          <?php
            $pos=mysqli_query($connect, "SELECT id_pelanggan, nama from t_pelanggan WHERE status = 'aktif' order by id_pelanggan");
            while($r_pos=mysqli_fetch_array($pos)){
              $selected = $r_pos['id_pelanggan'] === $lihat['id_pelanggan'] ? "selected = 'selected'" : '';
              echo "<option value=\"$r_pos[id_pelanggan]\" data-value='{\"id\": \"$r_pos[id_pelanggan]\", \"name\": \"$r_pos[nama]\"}' $selected>$r_pos[id_pelanggan]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Pelanggan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_pelanggan" value="<?php echo $lihat['nama'] ;?>" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Paket</label>
      <div class="col-sm-3">
        <!-- <input type="text" class="form-control" name="id_paket" value="<?php echo $lihat['id_paket'] ;?>"> -->
        <select name="kodePaket" onchange="showUser(this.value)" class="form-control" id="searchNamaPaket">
          <option value="" data-value="">--Pilih Paket--</option>
          <?php
            $pos=mysqli_query($connect, "SELECT id_paket, nama_paket, harga_paket from t_paket order by nama_paket");
            while($r_pos=mysqli_fetch_array($pos)){
              $selected = $r_pos['id_paket'] === $lihat['id_paket'] ? "selected = 'selected'" : '';
              echo "<option value=\"$r_pos[id_paket]\" data-value='{\"id\": \"$r_pos[id_paket]\", \"name\": \"$r_pos[nama_paket]\", \"harga_paket\": \"$r_pos[harga_paket]\"}' $selected >$r_pos[nama_paket]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <!-- <div class="form-group">
      <label class="col-sm-2 control-label">Nama Paket</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_paket" value="<?php echo $lihat['nama_paket'] ;?>" readonly>
      </div>
    </div> -->
    <div class="form-group">
      <label class="col-sm-2 control-label">Harga</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="harga_paket" value="<?php echo $lihat['harga_paket'] ;?>" readonly>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">Keterangan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="keterangan" value="<?php echo $lihat['keterangan'] ;?>">
      </div>
    </div>
    <!-- <div class="form-group">
      <label class="col-sm-2 control-label">Tagihan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="harga" value="<?php echo $lihat['harga'] ;?>">
    </div> -->
    
    
   <input type="hidden" name="id_tagihan" value="<?php echo "$_SESSION[id]" ;?>">
    <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
        <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Simpan</button>
        <a href="?page=tagihan" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
      </div>
    </div>
  </fieldset>


</form>

<?php
};
?>

  <?php 
  if(isset($_POST['simpan'])){
    $query=mysqli_query($connect, "UPDATE t_tagihan SET id_pelanggan='$_POST[kodePelanggan]', id_paket='$_POST[kodePaket]', tanggal='$_POST[tanggal]', keterangan='$_POST[keterangan]' WHERE id_tagihan='$_GET[id]'")or die(mysqli_error($connect));
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=tagihan">';
    } 


  ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
<link rel="stylesheet" href="css/chosen-bootstrap.css">

<script>
  $("#searchKodePelanggan").chosen().change(function() {
      $("input[name*='nama_pelanggan']").val($('#searchKodePelanggan option:selected').data("value").name);
  });

  $("#searchNamaPaket").chosen().change(function() {
      $("input[name*='harga_paket']").val($('#searchNamaPaket option:selected').data("value").harga_paket);
  });
</script>