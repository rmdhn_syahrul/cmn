<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Tagihan</li>
</ul>
 <?php
 include "./inc/config.php";
 $query=mysqli_query($connect, "SELECT t_tagihan.*, t_pelanggan.*, t_paket.* from t_tagihan left join t_pelanggan on t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan left join t_paket on t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.id_tagihan='$_GET[id]'");
 $result=mysqli_fetch_array($query);
 ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Detail Tagihan Pelanggan</h3>
  </div>
  <div class="panel-body">
	<div class="form-horizontal" role="form">
		<div class="form-group">
			<label class="col-sm-2 control-label">ID Tagihan</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['id_tagihan']; ?></label>
			</div>
		</div>					  
		<div class="form-group">
			<label class="col-sm-2 control-label">Tanggal Tagihan</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['tanggal']; ?></label>
			</div>
		</div>					  
		<div class="form-group">
			<label class="col-sm-2 control-label">ID Pelanggan</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['id_pelanggan']; ?></label>
			</div>
		</div>					  
		<div class="form-group">
			<label class="col-sm-2 control-label">Nama Pelanggan</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['nama']; ?></label>
			</div>
		</div>					  
		<div class="form-group">
			<label class="col-sm-2 control-label">Alamat</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['alamat']; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">No. Telp</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['no_hp']; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['email']; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Jenis Paket</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo $result['nama_paket']; ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Harga Paket</label>
			<div class="col-sm-10">
				<label class="col-sm-0 control-label">:</label>
				<label class="col-sm-0 control-label"><?php echo "Rp.".number_format($result['harga_paket'], 0, ',', '.'); ?></label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Status Tagihan</label>
			<div class="col-sm-10">
			<label class="col-sm-0 control-label">:</label>
			<?php if ($result['status_tagihan']=='LUNAS'){ ?>
				<span class="col-sm-0 control-label label label-success"><?php echo ucfirst($result['status_tagihan']) ?></span>
				<?php }else{ ?>
				<span class="col-sm-0 control-label label label-danger"><?php echo ucfirst($result['status_tagihan']) ?></span>
			<?php }?>
			</div>
		</div>
		<div class="btn-group pull-right">
		<?php 
			echo "<a href=\"?page=tagihan\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-arrow-left\"></span> Kembali</a>"; 
		?> 
		</div>
	</div>
  </div>
</div>


					