<?php
error_reporting(0);
session_start();
if ($_SESSION['level'] == "pelanggan") {
  header("location:index.php");
} else {
?>

  <ul class="breadcrumb">
    <li><a href="./">Home</a></li>
    <li class="active"><?php echo ucfirst($page); ?></li>
  </ul>
  <div class="btn-group">
    <a href="?page=pelanggan&aksi=tambah" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
  </div>

  <br /><br />
  <?php
  if ($action == "") {
  ?>

    <div class="table-responsive">
      <table class="table table-bordered" id="tabel-pelanggan">
        <thead>
          <tr class="info">
            <!-- <th>#</th> -->
            <th>ID Pelanggan</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>No HP</th>
            <!-- <th>Area</th> -->
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>

  <?php
  } else if ($action == "delete") {
    $hapus = mysqli_query($connect, "DELETE from t_pelanggan WHERE id_pelanggan='$_GET[id]'") or die(mysqli_error($connect));
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=pelanggan">';
    break;
  } else {
    echo "maaf aksi tidak ditemukan";
  }
  ?>
<?php } ?>

<!-- datatables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<script type="text/javascript">
  var tabel = null;

  $(document).ready(function() {
    tabel = $('#tabel-pelanggan').DataTable({
      "processing": true,
      "serverSide": true,
      "ordering": true,
      "order": [
        [0, 'asc']
      ],
      "ajax": {
        "url": "./view/datatables_pelanggan.php",
        "type": "POST"
      },
      "deferRender": true,
      "aLengthMenu": [
        [5, 10, 50],
        [5, 10, 50]
      ],
      "columns": [
        {
          "data": "id_pelanggan"
        }, 
        {
          "data": "nama"
        },
        {
          "data": "alamat"
        },
        {
          "data": "no_hp"
        },
        {
          "data": "status"
        },
        {
          "render": function(data, type, row) {
            var html = "<a href=\"?page=pelanggan&aksi=detail&id=" + row.id_pelanggan + " \"class=\"btn btn-success btn-sm\" title=\"Lihat Data\"><span class=\"glyphicon glyphicon-new-window\" aria-hidden=\"true\"></span></a>";
            html += "<a href=\"?page=pelanggan&aksi=edit&id=" + row.id_pelanggan + " \"class=\"btn btn-info btn-sm\" title=\"Edit Data\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a>";
              html += "<a href=\"?page=pelanggan&aksi=delete&id="+row.id_pelanggan+" \"class=\"btn btn-danger btn-sm\" title=\"Delete Data\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a>";  
            return html
          }
        },
      ],
      columnDefs: [{
        className: 'text-center',
        targets: [4, 5]
      }, {
        "targets":[2, 3, 5],
        "orderable":false,
      }]
    });
  });
</script>