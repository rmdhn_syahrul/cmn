<?php
include "./inc/config.php";
include "./inc/function.php";
$id = $_SESSION['id'];
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>

<form class="form-horizontal" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend>Tambah Data Transaksi</legend>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Transaksi</label>
      <div class="col-sm-3">
        <?php
          $query =mysqli_query($connect, "SELECT max(id_transaksi) as maxCode from t_transaksi");
          $data = mysqli_fetch_array($query);
          $maxData = $data['maxCode'];

          $urutan = (int) substr($maxData, 4, 3);
          $urutan++;

          $huruf = "TRX-";
          $kodeTransaksi = $huruf . sprintf("%03s", $urutan);
				?>
        <input type="text" class="form-control" name="id" required placeholder="ID Transaksi" value="<?php echo $kodeTransaksi;?>" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">No Tagihan</label>
      <div class="col-sm-3">
        <select name="nomorTagihan" onchange="showUser(this.value)" class="form-control" id="searchNomorTagihan">
          <option value="" data-value="">--Pilih Tagihan--</option>
          <?php
            if($_SESSION['level'] == 'admin'){
              $pos=mysqli_query($connect, "SELECT t_tagihan.id_tagihan, t_tagihan.tanggal, t_pelanggan.id_pelanggan, t_pelanggan.nama, t_paket.id_paket, t_paket.nama_paket, t_paket.harga_paket, t_tagihan.status_tagihan FROM t_tagihan JOIN t_pelanggan ON t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan JOIN t_paket ON t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.status_tagihan='PENDING'");
            } else {
              $pos=mysqli_query($connect, "SELECT t_tagihan.id_tagihan, t_tagihan.tanggal, t_pelanggan.id_pelanggan, t_pelanggan.nama, t_paket.id_paket, t_paket.nama_paket, t_paket.harga_paket, t_tagihan.status_tagihan FROM t_tagihan JOIN t_pelanggan ON t_tagihan.id_pelanggan=t_pelanggan.id_pelanggan JOIN t_paket ON t_tagihan.id_paket=t_paket.id_paket WHERE t_tagihan.status_tagihan='PENDING' and t_tagihan.id_pelanggan='$_GET[id]'");
            }
            while($r_pos=mysqli_fetch_array($pos)){
              echo "<option value=\"$r_pos[id_tagihan]\" data-value='{
                \"id\": \"$r_pos[id_tagihan]\", 
                \"tanggal\": \"$r_pos[tanggal]\",
                \"id_pelanggan\": \"$r_pos[id_pelanggan]\",
                \"nama_pelanggan\": \"$r_pos[nama]\",
                \"nama_paket\": \"$r_pos[nama_paket]\",
                \"harga_paket\": \"$r_pos[harga_paket]\"}'
              >$r_pos[id_tagihan]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal Tagihan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="tanggal" placeholder="Tanggal Tagihan" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Pelanggan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_pelanggan" required placeholder="ID Pelanggan" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Pelanggan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_pelanggan" required placeholder="Nama Pelanggan" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Paket</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_paket" required placeholder="Nama Pelanggan" readonly>
      </div>
    </div>
      <div class="form-group">
      <label class="col-sm-2 control-label">Harga Paket</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="inputku" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="harga_paket" placeholder="Harga Paket" readonly>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal Bayar</label>
      <div class="col-sm-3">
        <input type="text" id="datepicker" class="form-control" name="tgl_bayar" placeholder="Tanggal Bayar">
      </div>
    </div>
    <!-- <?php if($_SESSION['level'] == 'admin'){ ;?>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal Validasi</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="tgl_validasi" placeholder="Tanggal Validasi">
      </div>
    </div>
    <?php }; ?> -->
    <div class="form-group">
      <label class="col-sm-2 control-label">Jumlah Bayar</label>
      <div class="col-sm-3">        
        <input type="text" class="form-control" id="inputku" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="jumlah_bayar" placeholder="Jumlah Bayar">
      </div>
    </div>    
    <div class="form-group">
      <label class="col-sm-2 control-label">Bukti Pembayaran</label>
      <div class="col-sm-3">
        <input type="file" id="exampleInputFile" name="file" class="form-control">
      </div>
    </div>
    <div class="form-group">
	    <label class="col-sm-2 control-label">Status</label>
	    <div class="col-sm-2">
	    	<select name="statusTransaksi" onchange="showUser(this.value)" class="form-control">
				<option value="">--Pilih Status--</option>
				<option value="LUNAS">LUNAS</option>
				<option value="PENDING">PENDING</option>
		</select>
	    </div>
	  </div>
    
   <input type="hidden" name="info" value="1">
    <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
        <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tambah</button>
        <a href="?page=transaksi" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
      </div>
    </div>
  </fieldset>
</form>


  <?php 
  $maxsize = 1024 * 200; // maksimal 200 KB (1KB = 1024 Byte)
  $valid_ext = array('jpg','jpeg','png','gif','bmp');
  if(isset($_POST['simpan'])){
    // $ext = strtolower(end(explode('.', $_POST['file']['name'])));
    $cekdata="SELECT id_transaksi from t_transaksi where id_transaksi='".$_POST['id']."'"; 
    $ada=mysqli_query($connect, $cekdata) or die(mysqli_error($connect)); 
    $data="SELECT * from t_transaksi";
    $aya=mysqli_query($connect, $data) or die(mysqli_error($connect));
    if(mysqli_num_rows($ada)>0 && in_array($ext, $valid_array)) { 
      writeMsg('invoice.sama');
    }  else { 
      $query="INSERT INTO t_transaksi (id_transaksi, id_tagihan, tgl_bayar, nominal_bayar, status_transaksi) VALUES ('".$_POST['id']."','".$_POST['nomorTagihan']."','".$_POST['tgl_bayar']."','".str_replace(".","",$_POST['jumlah_bayar'])."','".$_POST['statusTransaksi']."')"; 
      mysqli_query($connect, $query) or die(mysqli_error($connect)); 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=transaksi">';
    } 
  } 

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
<link rel="stylesheet" href="css/chosen-bootstrap.css">

<script>
  $("#searchNomorTagihan").chosen().change(function() {
    $("input[name*='tanggal']").val($('#searchNomorTagihan option:selected').data("value").tanggal);
    $("input[name*='id_pelanggan']").val($('#searchNomorTagihan option:selected').data("value").id_pelanggan);
    $("input[name*='nama_pelanggan']").val($('#searchNomorTagihan option:selected').data("value").nama_pelanggan);
    $("input[name*='nama_paket']").val($('#searchNomorTagihan option:selected').data("value").nama_paket);
    $("input[name*='harga_paket']").val($('#searchNomorTagihan option:selected').data("value").harga_paket)
  });
</script>