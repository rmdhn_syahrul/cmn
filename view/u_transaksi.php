<?php
include "./inc/config.php";
include "./inc/function.php";
$id = $_SESSION['id'];

?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>
      <?php
        include "./inc/config.php";
        $query=mysqli_query($connect, "SELECT
        t_transaksi.*,
        t_tagihan.*,
        t_pelanggan.*,
        t_paket.*
        FROM
        t_transaksi
        LEFT JOIN t_tagihan ON t_transaksi.id_tagihan = t_tagihan.id_tagihan
        LEFT JOIN t_pelanggan ON t_tagihan.id_pelanggan = t_pelanggan.id_pelanggan
        LEFT JOIN t_paket ON t_tagihan.id_paket = t_paket.id_paket
        WHERE id_transaksi='$_GET[id]' " ) or die (mysqli_error($connect));  //mengambil data tabel mahasiswa dan memasukkan nya ke variabel query
        $no=1;                    //membuat nomor pada tabel
        while($lihat=mysqli_fetch_array($query)){    //mengeluarkan isi data dengan mysql_fetch_array dengan perulangan
      ?> 
<form class="form-horizontal" method="POST">
  <fieldset>
    <legend>Update Data Transaksi</legend>
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Transaksi</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" readonly name="id" required value="<?php echo $lihat['id_transaksi'] ;?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">No Invoice</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" readonly name="id" required value="<?php echo $lihat['id_tagihan'] ;?>">
      </div>
    </div>
      
    <div class="form-group">
      <label class="col-sm-2 control-label">ID Pelanggan</label>
      <div class="col-sm-3">
        <!-- <input type="text" class="form-control" name="id_pelanggan" value="<?php echo $lihat['id_pelanggan'] ;?>"> -->
        <select name="kodePelanggan" onchange="showUser(this.value)" class="form-control" id="searchKodePelanggan">
          <option value="" data-value="">--Pilih Pelanggan--</option>
          <?php
            $pos=mysqli_query($connect, "SELECT id_pelanggan, nama from t_pelanggan WHERE status = 'aktif' order by id_pelanggan");
            while($r_pos=mysqli_fetch_array($pos)){
              $selected = $r_pos['id_pelanggan'] === $lihat['id_pelanggan'] ? "selected = 'selected'" : '';
              echo "<option value=\"$r_pos[id_pelanggan]\" data-value='{\"id\": \"$r_pos[id_pelanggan]\", \"name\": \"$r_pos[nama]\"}' $selected>$r_pos[id_pelanggan]</option>";
            }
          ?>
      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Nama Pelanggan</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_pelanggan" value="<?php echo $lihat['nama'] ;?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal Bayar</label>
      <div class="col-sm-3">
        <input type="text" id="datepicker" class="form-control" name="tgl_bayar" value="<?php echo $lihat['tgl_bayar'] ;?>">
      </div>
    </div>
    <?php if($_SESSION['level'] == 'admin'){ ;?>
    <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal Validasi</label>
      <div class="col-sm-3">
        <input type="text" id="tgl_validasi" class="form-control" name="tgl_validasi" value="<?php echo $lihat['tgl_validasi'] ;?>">
      </div>
    </div>
    <?php }; ?>
    <div class="form-group">
      <label class="col-sm-2 control-label">Harga Paket</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" readonly name="harga" required value="<?php echo $lihat['harga_paket'] ;?>">
      </div>
    </div>
      <!-- <div class="form-group">
      <label class="col-sm-2 control-label">Area</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" readonly name="area" required value="<?php echo $lihat['nama_area'] ;?>">
      </div>
    </div> -->
    <div class="form-group">
      <label class="col-sm-2 control-label">Jumlah Bayar</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nominal" value="<?php echo $lihat['nominal_bayar'] ;?>">
      </div>
    </div>
    <?php if($_SESSION['level'] == 'admin'){ ;?>  
    <div class="form-group">
      <label class="col-sm-2 control-label">Status</label>
      <div class="col-sm-3">
        <select name="status" class="form-control">
          <option value="">--Pilih Status--</option>
          <option <?php if( $lihat['status_transaksi']=='LUNAS'){echo "selected"; } ?> value='LUNAS'>Lunas</option>
          <option <?php if( $lihat['status_transaksi']=='PENDING'){echo "selected"; } ?> value='PENDING'>Pending</option>          
        </select>
      </div>
    </div>  
    <?php }; ?>
    <?php if($_SESSION['level'] == 'pelanggan'){ ;?>   
    <div class="form-group">
      <label class="col-sm-2 control-label">Bukti Pembayaran</label>
      <div class="col-sm-3">
        <input type="file" id="exampleInputFile" name="file">
      </div>
    </div> 
    <?php }; ?>
   
   <input type="hidden" name="id_pelanggan" value="<?php echo "$_SESSION[id]" ;?>">
    <div class="form-group">
      <div class="col-sm-10 col-sm-offset-2">
        <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Simpan</button>
        <a href="?page=transaksi" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
      </div>
    </div>
  </fieldset>


</form>
<?php
};
?>

  <?php 
  if(isset($_POST['simpan'])){
    if($_SESSION['level'] == 'admin'){
      $query=mysqli_query($connect, "UPDATE t_transaksi SET tgl_bayar='$_POST[tgl_bayar]', tgl_validasi='$_POST[tgl_validasi]', nominal_bayar='$_POST[nominal]', status_transaksi='$_POST[status]' WHERE id_transaksi='$_POST[id]'") or die(mysqli_error($connect));
    }else{
      $query=mysqli_query($connect, "UPDATE t_transaksi SET tgl_bayar='$_POST[tgl_bayar]', tgl_validasi='$_POST[tgl_validasi]', nominal_bayar='$_POST[nominal]' WHERE id_transaksi='$_POST[id]'") or die(mysqli_error($connect));
    }
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=transaksi">';
  } 


  ?>

<script type="text/javascript">
  $("#searchKodePelanggan").chosen().change(function() {
      $("input[name*='nama_pelanggan']").val($('#searchKodePelanggan option:selected').data("value").name);
  });
</script>