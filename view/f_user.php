<?php
  session_start();
  include "./inc/config.php";
  include "./inc/function.php";
  if($_SESSION['level']=="operator"){
  header("location:index.php");
}else{
?>
<ul class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=<?php echo $page ;?>"><?php echo ucfirst($page) ; ?></a></li>
  <li class="active"><?php echo ucfirst($action) ; ?> Data</li>
</ul>
<fieldset>
	<legend>Tambah Data Pelanggan</legend>
	<form class="form-horizontal"  method="post">	  
	  <div class="form-group">
	    <label class="col-sm-2 control-label">ID Pelanggan</label>
	    <div class="col-sm-4">
	      	<select name="id" onchange="showUser(this.value)" class="form-control" id="searchIDPelanggan">
				<option value="" data-value="">--Pilih ID Pelanggan--</option>
				<?php
					$pos=mysqli_query($connect, "SELECT * from t_pelanggan order by id_pelanggan");
					while($r_pos=mysqli_fetch_array($pos)){
						echo "<option value=\"$r_pos[id_pelanggan]\" data-value='{
							\"id\": \"$r_pos[id_pelanggan]\", 
							\"nama\": \"$r_pos[nama]\"}'>$r_pos[id_pelanggan]</option>";
					}
                ?>
			</select>
	    </div>
	  </div> 
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Nama Pelanggan</label>
	    <div class="col-sm-3">
	      <input type="text" class="form-control" name="nama" placeholder="Nama Pelanggan" readonly>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Username</label>
	    <div class="col-sm-3">
	      <input type="text" class="form-control" name="username" placeholder="Username">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Password</label>
	    <div class="col-sm-3">
	      <input type="password" class="form-control" name="password" placeholder="Password">
	    </div>
	  </div> 
	  <div class="form-group">
	    <label class="col-sm-2 control-label">Level</label>
	    <div class="col-sm-3">
	      	<select name="level" class="form-control">
				<option value="">--Pilih Level--</option>
				<option value="admin">Admin</option>
				<option value="pelanggan">Pelanggan</option>
			</select>
	    </div>
	  </div> 
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	    <button type="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset</button>
        <button type="submit" name="simpan" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tambah</button>
        <a href="?page=user" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Batal </a>
	    </div>
	  </div>
	</form>
</fieldset>

  <?php 
  if(isset($_POST['simpan'])){
    $cekdata="SELECT id_pelanggan from t_user where id_pelanggan='".$_POST['id']."'"; 
    $ada=mysqli_query($connect, $cekdata) or die(mysqli_error($connect)); 
    if(mysqli_num_rows($ada)>0) { 
      writeMsg('pelanggan.sama');
    } else { 
      $query="INSERT INTO t_user VALUES ('".$_POST['id']."','".$_POST['username']."','".md5($_POST['password'])."','".$_POST['level']."')"; 
      mysqli_query($connect, $query) or die("Gagal menyimpan data karena :") or die(mysqli_error($connect)); 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL=?page=user">';
    } 
  } 

  ?>

<?php
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
<link rel="stylesheet" href="css/chosen-bootstrap.css">

<script>
  $("#searchIDPelanggan").chosen().change(function() {
    $("input[name*='nama']").val($('#searchIDPelanggan option:selected').data("value").nama);
  });
</script>