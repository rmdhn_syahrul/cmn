-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 17 Agu 2020 pada 18.41
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `pt_cmn`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id_area` varchar(11) NOT NULL,
  `nama_area` varchar(50) NOT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `area`
--

INSERT INTO `area` (`id_area`, `nama_area`) VALUES
('1', 'CENGKARENG'),
('2', 'SALEMBA'),
('3', 'Persakih'),
('5', 'CIKUNIR');

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_transaksi`
--

CREATE TABLE IF NOT EXISTS `d_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_bayar` int(11) NOT NULL,
  `nominal` int(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `harga` varchar(11) NOT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_login`
--

CREATE TABLE IF NOT EXISTS `t_login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `level` enum('admin','operator') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_2` (`username`),
  KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_login`
--

INSERT INTO `t_login` (`id`, `username`, `password`, `nama_lengkap`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin Markimin', 'admin'),
(2, 'operator', 'ee11cbb19052e40b07aac0ca060c23ee', 'Operator', 'operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_paket`
--

CREATE TABLE IF NOT EXISTS `t_paket` (
  `id_paket` varchar(3) NOT NULL,
  `nama_paket` varchar(25) NOT NULL,
  `harga` varchar(7) NOT NULL,
  PRIMARY KEY (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_paket`
--

INSERT INTO `t_paket` (`id_paket`, `nama_paket`, `harga`) VALUES
('P01', 'INDIHOME', '200000'),
('P02', '512 Kbps', '250000'),
('P03', 'XL', '230000'),
('P04', '2 GIGABITE', '420000'),
('P05', 'SIMPATI', '320000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pelanggan`
--

CREATE TABLE IF NOT EXISTS `t_pelanggan` (
  `id_pelanggan` varchar(25) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(25) NOT NULL,
  `id_paket` varchar(3) NOT NULL,
  `status` enum('aktif','non aktif','','') NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_pelanggan`
--

INSERT INTO `t_pelanggan` (`id_pelanggan`, `nama`, `alamat`, `no_hp`, `email`, `id_paket`, `status`) VALUES
('SA0101', 'admin', 'jl bahagia', '0210003442', 'admin@emailku.com', 'P02', 'aktif'),
('SN01', 'dedy', 'jl.cengkareng', '089924255', 'dedyspd@gmail.com', 'P02', 'aktif'),
('SN02', 'Afika Aliyah', 'Kalapajajar', '0812', 'afika@email.com', 'P01', 'aktif'),
('SN03', 'User', 'Ciamis', '082', 'email@email.com', 'P01', 'aktif'),
('SN04', 'Coba', 'Handapherang, Ciamis', '0812345', 'coba@emai.com', 'P02', 'non aktif'),
('SN05', 'wira', 'asdasdasdasfww', '2344353232', 'adsasfasaa@aaasfe.com', 'P02', 'aktif'),
('SN06', 'jawir', 'bojong', '089266673', 'jawir@kiwir', 'P01', 'aktif'),
('SN07', 'wfawf', 'qeerww', '0899242', 'safas@saga', '005', 'aktif'),
('SN08', 'jojo', 'weewqww', '089555466', 'jojo@jeje', '007', 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_setting`
--

CREATE TABLE IF NOT EXISTS `t_setting` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `logo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `t_setting`
--

INSERT INTO `t_setting` (`id`, `nama`, `alamat`, `pemilik`, `logo`) VALUES
(1, 'PT.Ciptakom Media Nusa', 'Komplek Imigrasi Kertapawitan No.134', 'Subhi Wirasaba', 'logo.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_tagihan`
--

CREATE TABLE IF NOT EXISTS `t_tagihan` (
  `id_tagihan` varchar(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` enum('lunas','pending','','') NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_tagihan`
--

INSERT INTO `t_tagihan` (`id_tagihan`, `nama`, `tanggal`, `keterangan`, `harga`) VALUES
('SN02', 'afika', '2020-05-23', 'lunas', 200000),
('SN03', 'user', '2020-05-23', 'lunas', 450000),
('SN04', 'dedy', '2020-06-26', 'lunas', 400000),
('SN05', 'Wira', '2020-05-23', 'lunas', 75000),
('SN06', 'jawir', '2020-05-23', 'lunas', 200000),
('SN07', 'wfawf', '2020-05-23', 'pending', 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_transaksi`
--

CREATE TABLE IF NOT EXISTS `t_transaksi` (
  `id_transaksi` varchar(30) NOT NULL,
  `id_pelanggan` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `nominal` int(7) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `tgl_validasi` date NOT NULL,
  `status` enum('pending','lunas') NOT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_transaksi`
--

INSERT INTO `t_transaksi` (`id_transaksi`, `id_pelanggan`, `harga`, `nominal`, `bukti`, `tgl_bayar`, `tgl_validasi`, `status`) VALUES
('INV001', 'SN02', 0, 200000, '', '2020-05-23', '2020-05-23', 'lunas'),
('INV0014', 'SN05', 0, 75000, '', '2020-05-01', '2020-05-31', 'lunas'),
('INV002', 'SN01', 0, 75000, '', '2020-06-03', '2020-06-03', 'lunas'),
('INV003', 'SA0101', 0, 200000, '', '2020-08-17', '0000-00-00', 'pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id_pelanggan` varchar(25) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `level` enum('admin','pelanggan') NOT NULL,
  UNIQUE KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_user`
--

INSERT INTO `t_user` (`id_pelanggan`, `username`, `password`, `level`) VALUES
('SA0101', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
('SN01', 'dedy', '13bbf54a6850c393fb8d1b2b3bba997b', 'pelanggan'),
('SN02', 'afika', 'd94ac5196f76d7562f5f9fd7ab478484', 'pelanggan'),
('SN03', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'pelanggan'),
('SN05', 'wira', '6215f4770ee800ad5402bc02be783c26', 'pelanggan'),
('SN06', 'jawir', '955617817b2d33cbfa0285b3b1789a27', 'pelanggan'),
('SN07', 'wfa', '049345ffd8a1908f04c880a7937b4420', 'pelanggan');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `t_user`
--
ALTER TABLE `t_user`
  ADD CONSTRAINT `t_user_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `t_pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
