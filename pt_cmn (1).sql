-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2020 at 06:59 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pt_cmn`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id_area` varchar(11) NOT NULL,
  `nama_area` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id_area`, `nama_area`) VALUES
('1', 'CENGKARENG'),
('2', 'SALEMBA'),
('3', 'Persakih'),
('5', 'CIKUNIR');

-- --------------------------------------------------------

--
-- Table structure for table `d_transaksi`
--

CREATE TABLE `d_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_bayar` int(11) NOT NULL,
  `nominal` int(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `harga` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_login`
--

CREATE TABLE `t_login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `level` enum('admin','operator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_login`
--

INSERT INTO `t_login` (`id`, `username`, `password`, `nama_lengkap`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin Markimin', 'admin'),
(2, 'operator', 'ee11cbb19052e40b07aac0ca060c23ee', 'Operator', 'operator');

-- --------------------------------------------------------

--
-- Table structure for table `t_paket`
--

CREATE TABLE `t_paket` (
  `id_paket` varchar(3) NOT NULL,
  `nama_paket` varchar(25) NOT NULL,
  `harga_paket` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_paket`
--

INSERT INTO `t_paket` (`id_paket`, `nama_paket`, `harga_paket`) VALUES
('P01', 'INDIHOME', '20000'),
('P02', '512 Kbps', '250000'),
('P03', 'XL', '230000'),
('P04', '2 GIGABITE', '420000'),
('P05', 'SIMPATI', '320000'),
('XL8', 'XL UNLIMITED 80GB', '120000');

-- --------------------------------------------------------

--
-- Table structure for table `t_pelanggan`
--

CREATE TABLE `t_pelanggan` (
  `id_pelanggan` varchar(25) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(25) NOT NULL,
  `status` enum('aktif','non aktif','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pelanggan`
--

INSERT INTO `t_pelanggan` (`id_pelanggan`, `nama`, `alamat`, `no_hp`, `email`, `status`) VALUES
('PEL004', 'Audrey', 'Rumahnye Kylie Jenner', '00', 'mail@email.com', 'aktif'),
('PEL006', 'Nama 6', 'Alamat 6', '889907654', 'mail@email.com', 'aktif'),
('PEL007', 'Nama Pel 7', 'Alamat Pel 7', '11224357', 'mail7@gmail.com', 'aktif'),
('PEL102', 'user', 'alamat', '112233445', 'mail@email.com', 'aktif'),
('SA0101', 'admin', 'jl bahagia', '0210003442', 'admin@emailku.com', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `t_setting`
--

CREATE TABLE `t_setting` (
  `id` int(1) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_setting`
--

INSERT INTO `t_setting` (`id`, `nama`, `alamat`, `pemilik`, `logo`) VALUES
(1, 'PT.Ciptakom Media Nusa', 'Komplek Imigrasi Kertapawitan No.134A', 'Subhi Wirasaba', 'logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_tagihan`
--

CREATE TABLE `t_tagihan` (
  `id_tagihan` varchar(11) NOT NULL,
  `id_pelanggan` varchar(20) NOT NULL,
  `id_paket` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `status_tagihan` enum('LUNAS','PENDING',',') NOT NULL DEFAULT 'PENDING',
  `total_tagihan` int(11) NOT NULL,
  `keterangan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tagihan`
--

INSERT INTO `t_tagihan` (`id_tagihan`, `id_pelanggan`, `id_paket`, `tanggal`, `status_tagihan`, `total_tagihan`, `keterangan`) VALUES
('INV-001', 'PEL004', 'P02', '2020-09-01', 'LUNAS', 420000, ''),
('INV-002', 'PEL006', 'P01', '2020-09-01', 'LUNAS', 200000, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_transaksi`
--

CREATE TABLE `t_transaksi` (
  `id_transaksi` varchar(30) NOT NULL,
  `id_tagihan` varchar(30) NOT NULL,
  `nominal_bayar` int(7) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `tgl_validasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_transaksi` enum('PENDING','LUNAS','') NOT NULL DEFAULT 'LUNAS'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_transaksi`
--

INSERT INTO `t_transaksi` (`id_transaksi`, `id_tagihan`, `nominal_bayar`, `bukti`, `tgl_bayar`, `tgl_validasi`, `status_transaksi`) VALUES
('TRX-001', 'INV-001', 250000, '', '2020-09-01', '2020-08-31 18:41:04', 'LUNAS'),
('TRX-002', 'INV-002', 56000, '', '2020-09-01', '2020-08-31 19:23:37', 'LUNAS');

--
-- Triggers `t_transaksi`
--
DELIMITER $$
CREATE TRIGGER `transaction_trig` AFTER INSERT ON `t_transaksi` FOR EACH ROW begin
       DECLARE id_exists Boolean;
       -- Check BookingRequest table
       SELECT 1
       INTO @id_exists
       FROM t_tagihan
       WHERE t_tagihan.id_tagihan = NEW.id_tagihan;

       IF @id_exists = 1
       THEN
           UPDATE t_tagihan
           SET status_tagihan = NEW.status_transaksi
           WHERE id_tagihan = NEW.id_tagihan;
        END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_pelanggan` varchar(25) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `level` enum('admin','pelanggan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_pelanggan`, `username`, `password`, `level`) VALUES
('PEL004', 'audrey', '8c08dc529eccf4277a402cb8f7da0c96', 'pelanggan'),
('PEL102', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'pelanggan'),
('SA0101', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id_area`);

--
-- Indexes for table `d_transaksi`
--
ALTER TABLE `d_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `t_login`
--
ALTER TABLE `t_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `t_paket`
--
ALTER TABLE `t_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `t_pelanggan`
--
ALTER TABLE `t_pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `t_setting`
--
ALTER TABLE `t_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tagihan`
--
ALTER TABLE `t_tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- Indexes for table `t_transaksi`
--
ALTER TABLE `t_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD UNIQUE KEY `id_pelanggan` (`id_pelanggan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_setting`
--
ALTER TABLE `t_setting`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_user`
--
ALTER TABLE `t_user`
  ADD CONSTRAINT `t_user_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `t_pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
